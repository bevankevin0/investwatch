//
//  CryptoService.swift
//  Investwatch
//
//  Created by bevan christian on 21/06/21.
//

import Foundation
import Combine


class CryptoService{
    @Published var listSaham:[CryptoListModel] = []
    private var cancelable:AnyCancellable?
    
    init() {
        downloadCrypto()
    }
    
    func downloadCrypto(){
        guard  let url = URL(string: "https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=volume_desc&per_page=120&page=1&sparkline=true&price_change_percentage=24h") else {return}
        cancelable = URLSession.shared.dataTaskPublisher(for: url)
            .subscribe(on: DispatchQueue.global(qos: .default))
            .tryMap{(output) -> Data in
                guard let response = output.response as? HTTPURLResponse,
                      response.statusCode >= 200 && response.statusCode < 300 else {
                    throw URLError(.badServerResponse)
                }
                return output.data
            }.receive(on: DispatchQueue.main)
            .decode(type: [CryptoListModel].self, decoder: JSONDecoder())
            .sink { (completion) in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    print("error hdhdhddhd\(error.localizedDescription)")
                }
            } receiveValue: {[weak self] (hasil) in
                self?.listSaham = hasil
                self?.cancelable?.cancel()
            }
        
        
        
    }
}
