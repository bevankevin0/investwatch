//
//  DetailService.swift
//  Investwatch
//
//  Created by bevan christian on 21/06/21.
//

import Foundation
import Combine


class DetailService {
    @Published var detailCrypto = CryptoDetailModel()
    private var cancelable:AnyCancellable?
    
//    init(idCryoto:String) {
//        downloadDetail(idCrypto: idCryoto)
//    }
    
    func downloadDetail(idCrypto:String){
        let url = URL(string: linkPenting.urlDetail(idCrypto).stringValue)!
        cancelable =  URLSession.shared.dataTaskPublisher(for: url)
            .subscribe(on: DispatchQueue.global(qos: .default))
            .tryMap{(output) -> Data in
                guard let response = output.response as? HTTPURLResponse,
                      response.statusCode >= 200 && response.statusCode < 300 else {
                    print("dari server ")

                    throw URLError(.badServerResponse)
                }
                return output.data
            }.receive(on: DispatchQueue.main).decode(type: CryptoDetailModel.self, decoder: JSONDecoder())
            .sink { completion in
                switch completion {
                case .finished:
                    print("selesai")
                case .failure(_):
                    print("error")
                }
            } receiveValue: { [weak self] (hasil) in
                self?.detailCrypto = hasil
                self?.cancelable?.cancel()
            }
        
        
        
    }
    
   
}
