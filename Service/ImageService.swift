//
//  ImageService.swift
//  Investwatch
//
//  Created by bevan christian on 21/06/21.
//

import Foundation
import SwiftUI
import Combine

class ImageService {
    @Published var image:UIImage? = nil
    var imageCache = ImageCache.getImageCache()
    private var cancelable:AnyCancellable?
    
    init(urlImage:String) {
        loadImage(urlImage: urlImage)
    }
    
    func loadImage(urlImage:String){
        if loadImageFromCache(url: urlImage) {
            return
        } // jika ga ada di chace maka download
        downloadImage(urlImage: urlImage)
    }
    
    
    func loadImageFromCache(url:String?) -> Bool {
        guard let urlString = url else {
            return false
        }
        
        guard let cacheImage = imageCache.get(forKey: urlString) else {
            return false
        }
        print("chace")
        image = cacheImage
        return true
    }
    func downloadImage(urlImage:String){
        print("download")
        let url = URL(string:urlImage)!
        cancelable =  URLSession.shared.dataTaskPublisher(for: url)
            .subscribe(on: DispatchQueue.global(qos: .default))
            .tryMap(\.data)
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case .finished:
                    print("selesai")
                case .failure(_):
                    print("error")
                }
            } receiveValue: { [weak self] (hasil) in
                self?.image = UIImage(data: hasil)
                self?.imageCache.set(forKey: urlImage, image: (self?.image)!)
                self?.cancelable?.cancel()
            }
        
        
        
    }
}
