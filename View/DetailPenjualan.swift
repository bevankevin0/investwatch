//
//  DetailTransaction.swift
//  Investwatch
//
//  Created by bevan christian on 22/06/21.
//

import SwiftUI

struct DetailPenjualan: View {
    @Environment(\.presentationMode) var presenter
    //@EnvironmentObject var cryptoVm:RequestVM
    @EnvironmentObject var transactionVm:TransactionVM
    @Binding var sellPage:Bool
    @EnvironmentObject var porto:PortofolioVM
    @State var crypto:CryptoListModel
    var indeksCrypto:Int
    @State var buy:String
    var body: some View {
        VStack {
            Text("Sell Preview").bold()
            Text("\(transactionVm.jumlahCryptoJual) \(crypto.id!)").font(.title).foregroundColor(.blue)
                .padding(.vertical, 50.0)
            
            VStack(alignment: .leading,spacing:15) {
                Section {
                    HStack{
                        Text("Price").font(.headline).foregroundColor(Color.theme.utama)
                        Spacer()
                        Text("$\(crypto.currentPrice!)").foregroundColor(Color.theme.kedua)
                        
                    }
                    Divider()
                    HStack{
                        Text("Your Average Price").font(.headline).foregroundColor(Color.theme.utama)
                        Spacer()
                        Text("\(crypto.hargaRataRata!)").foregroundColor(Color.theme.kedua)
                    }
                    Divider()
                }
                Section{
                    HStack{
                        Text("Total Asset").font(.headline).foregroundColor(Color.theme.utama)
                        Spacer()
                        Text("$\(crypto.jumlahDolar!)").foregroundColor(Color.theme.kedua)
                    }
                    Divider()
                    HStack{
                        Text("Your Return").font(.headline).foregroundColor(Color.theme.utama)
                        Spacer()
                        Text("\(Double(crypto.presentasePerubahan!)!)").foregroundColor(Color.theme.kedua)
                    }
                    Divider()
                    HStack{
                        Text("Total Coin You Have").font(.headline).foregroundColor(Color.theme.utama)
                        Spacer()
                        Text("\(crypto.jumlahCoin!)").foregroundColor(Color.theme.kedua)
                    }
                    Divider()
                        .padding(.bottom, 30.0)
                }
             
                HStack{
                    Text("Total").font(.title2).fontWeight(.bold).foregroundColor(Color.theme.utama)
                    Spacer()
                    Text("\(buy)").foregroundColor(Color.theme.kedua).font(.title2)
                }
            }.padding()
            Spacer()
            NavigationLink(destination: TransactionDone(buyPage: $sellPage)) {
                Text("Sell").frame(width:300 ,height: 40, alignment: .center).background(Color.blue).foregroundColor(.white).cornerRadius(10).padding()
                                }.simultaneousGesture(TapGesture().onEnded{
                                    transactionVm.sell(hargaNow: String(crypto.currentPrice!), jumlahJual: String(buy), idCrypto: String(crypto.id!), namaCrypto:String(crypto.name!), presentase: crypto.presentasePerubahan!)
                                    transactionVm.getPorto()
                                    porto.subsicreCrypto()
                                    porto.getPorto()
                                    porto.getTotal()
                            })
            
        }.onAppear(perform: {
            transactionVm.kalkulasiJual(hargaNow: String(crypto.currentPrice!), jumlahJual: buy)
        })
        
        
    }
}

//struct DetailTransaction_Previews: PreviewProvider {
//    @StateObject static var cryptoVM = RequestVM()
//    static var previews: some View {
//        DetailTransaction(coin: cryptoVM.listSaham[0], buy: "30").environmentObject(cryptoVM)
//    }
//}
