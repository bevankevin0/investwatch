//
//  SearchView.swift
//  Investwatch
//
//  Created by bevan christian on 21/06/21.
//

import SwiftUI

struct SearchView: View {
    @EnvironmentObject var cryptoVm:RequestVM
    @State var searchText = ""
    var body: some View {
        ZStack {
            Color.theme.background
            NavigationView{
                VStack{
                        TextField(" Search your coin", text: $cryptoVm.searchText).frame( height:35, alignment: .center).background(RoundedRectangle(cornerRadius: 5).fill(Color.theme.backgroundDua)).foregroundColor(Color.theme.utama).padding()
                            .onChange(of: cryptoVm.searchText, perform: { value in
                                cryptoVm.search(type: value)
                        })
                    List{
                        ForEach(cryptoVm.kontainer){ data in
                            NavigationLink(
                                destination: DetailCrypto(coin: data),
                                label: {
                                    CardList(price: String(data.currentPrice!), nama: data.name!, volume: String(data.totalVolume!), image: data, cryptoVm: cryptoVm )
                                }).frame(height:60)
                        }
                    }.listStyle(PlainListStyle()).ignoresSafeArea(.all)
                }
                .navigationTitle("Search Coin")
            }
        }.background(Color.theme.background.edgesIgnoringSafeArea(.all)).onAppear {
            cryptoVm.subsicreCrypto()
            cryptoVm.subscribeSearch()
        }
     
    }
}

struct SearchView_Previews: PreviewProvider {
    @StateObject static var model = RequestVM()
    static var previews: some View {
        SearchView().environmentObject(model)
    }
}
