//
//  sellpage.swift
//  Investwatch
//
//  Created by bevan christian on 22/06/21.
//

import SwiftUI

struct SellPage: View {
    @Environment(\.presentationMode) var presenter
    @State var crypto:CryptoListModel
    @State var pembelian = ""
    @State var melebihiKapasitas = false
    @Binding var sellPage:Bool
    var image:UIImage?
    var indeksCrypto:Int
    //var harga = 0
    var body: some View {
        NavigationView{
            VStack {
                VStack {
                    Text("Sell Preview").font(.headline).padding(.bottom, 2)
                    Text("Current Price $\(String(crypto.currentPrice!))").font(.subheadline)
                    HStack{
                        Text("Your Asset").font(.subheadline)
                        Text("\(crypto.jumlahDolar!)").font(.subheadline)
                    }
                    HStack(spacing:0) {
                        if image != nil {
                            Image(uiImage:image!).resizable().clipShape(Circle()).frame(width: 50, height: 50, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        }else {
                            Image("gmb1").resizable().clipShape(Circle()).frame(width: 50, height: 50, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        }
                        Text("\(crypto.name!)").bold().padding()
                    }.font(.title3)
                    
                    Spacer()
                    Text("$\(pembelian)").font(.title).foregroundColor(.green).onChange(of: pembelian, perform: { value in
                        if Double(pembelian ?? "0") ?? 0.0 > Double(crypto.jumlahDolar!)! {
                            melebihiKapasitas = true
                        }else {
                            melebihiKapasitas = false
                        }
                    })
                    Spacer()
                }
            
                Spacer(minLength: 50)
                Text(melebihiKapasitas ? "Not enough asset" : "").font(.caption2).foregroundColor(Color.theme.kedua)
                Divider().padding(.bottom)
                Keyboard(pembelian: $pembelian).padding(.bottom)
                if melebihiKapasitas {
                    Text("Sell").frame(width:300 ,height: 40, alignment: .center).background(Color.gray).foregroundColor(.white).cornerRadius(10).padding().disabled(true)
                } else {
                    NavigationLink(
                        destination: DetailPenjualan(sellPage: $sellPage, crypto: crypto, indeksCrypto: indeksCrypto, buy: pembelian),
                        label: {
                            Text("Sell").frame(width:300 ,height: 40, alignment: .center).background(Color.blue).foregroundColor(.white).cornerRadius(10).padding()
                        })
                }
         
               
            }.offset(y:-50)
            .navigationBarItems(leading: Button(action: {
                presenter.wrappedValue.dismiss()
            }, label: {
                Image(systemName: "multiply.circle.fill").foregroundColor(Color.theme.utama)
            }))
        }
    
    }
}

//struct BuyPage_Previews: PreviewProvider {
//    static var previews: some View {
//        BuyPage()
//    }
//}
