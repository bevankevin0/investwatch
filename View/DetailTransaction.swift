//
//  DetailTransaction.swift
//  Investwatch
//
//  Created by bevan christian on 22/06/21.
//

import SwiftUI

struct DetailTransaction: View {
    @Environment(\.presentationMode) var presenter
    @EnvironmentObject var cryptoVm:RequestVM
    @EnvironmentObject var transactionVm:TransactionVM
    @EnvironmentObject var porto:PortofolioVM
    @Binding var buyPage:Bool
    var indeksCrypto:Int
    @State var buy:String
    var body: some View {
        VStack {
            Text("Order Preview").bold()
            Text("\(transactionVm.jumlahCrypto) \(cryptoVm.listSaham[indeksCrypto].id!)").font(.title).foregroundColor(.blue)
                .padding(.vertical, 50.0)
            
            VStack(alignment: .leading,spacing:15) {
                HStack{
                    Text("Price").font(.headline).foregroundColor(Color.theme.utama)
                    Spacer()
                    Text("$\(String(format: "%.0f", cryptoVm.listSaham[indeksCrypto].currentPrice!))").foregroundColor(Color.theme.kedua)
                    
                }
                Divider()
                HStack{
                    Text("Current Market cap").font(.headline).foregroundColor(Color.theme.utama)
                    Spacer()
                    Text("\(String(format: "%.0f", cryptoVm.listSaham[indeksCrypto].marketCap!))").foregroundColor(Color.theme.kedua)
                }
                Divider()
                HStack{
                    Text("High 24H").font(.headline).foregroundColor(Color.theme.utama)
                    Spacer()
                    Text("\(String(format: "%.0f", cryptoVm.listSaham[indeksCrypto].high24H!))").foregroundColor(Color.theme.kedua)
                }
                Divider()
                HStack{
                    Text("Volume").font(.headline).foregroundColor(Color.theme.utama)
                    Spacer()
                    Text("\(String(format: "%.0f", cryptoVm.listSaham[indeksCrypto].totalVolume!))").foregroundColor(Color.theme.kedua)
                }
                Divider()
                    .padding(.bottom, 30.0)
                
                HStack{
                    Text("Total").font(.title2).fontWeight(.bold).foregroundColor(Color.theme.utama)
                    Spacer()
                    Text("\(buy)").foregroundColor(Color.theme.kedua).font(.title2)
                }
            }.padding()
            Spacer()
            NavigationLink(destination: TransactionDone(buyPage: $buyPage)) {
                Text("Buy").frame(width:300 ,height: 40, alignment: .center).background(Color.blue).foregroundColor(.white).cornerRadius(10).padding()
                                }.simultaneousGesture(TapGesture().onEnded{
                                    transactionVm.buy(hargaNow: String(cryptoVm.listSaham[indeksCrypto].currentPrice!), jumlahBeli: String(buy), idCrypto: String(cryptoVm.listSaham[indeksCrypto].id!), namaCrypto:
                                    String(cryptoVm.listSaham[indeksCrypto].name!))
                                    transactionVm.getPorto()
                                    porto.subsicreCrypto()
                                    porto.getPorto()
                                    porto.getTotal()
                            })
            
        }.onAppear(perform: {
            transactionVm.kalkulasi(hargaNow: String(cryptoVm.listSaham[indeksCrypto].currentPrice!), jumlahBeli: buy)
        })
        
        
    }
}

//struct DetailTransaction_Previews: PreviewProvider {
//    @StateObject static var cryptoVM = RequestVM()
//    static var previews: some View {
//        DetailTransaction(coin: cryptoVM.listSaham[0], buy: "30").environmentObject(cryptoVM)
//    }
//}
