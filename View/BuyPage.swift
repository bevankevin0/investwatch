//
//  BuyPage.swift
//  Investwatch
//
//  Created by bevan christian on 22/06/21.
//

import SwiftUI

struct BuyPage: View {
    @Environment(\.presentationMode) var presenter
    @EnvironmentObject var cryptoVm:RequestVM
    @State var pembelian = ""
    @Binding var buyPage:Bool
    var indeksCrypto:Int
    //var harga = 0
    var body: some View {
        NavigationView{
            VStack {
                VStack {
                    Text("Buy Preview").font(.headline).padding(.bottom, 2)
                    Text("Current Price $\(String(cryptoVm.listSaham[indeksCrypto].currentPrice!))").font(.subheadline)
                    HStack(spacing:0) {
                        if cryptoVm.listSaham[indeksCrypto].imageData != nil {
                            Image(uiImage: cryptoVm.listSaham[indeksCrypto].imageData!).resizable().clipShape(Circle()).frame(width: 50, height: 50, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        }else {
                            Image("gmb1").resizable().clipShape(Circle()).frame(width: 50, height: 50, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        }
                        Text("\(cryptoVm.listSaham[indeksCrypto].name!)").bold().padding()
                    }.font(.title3)
                    
                    Spacer()
                    Text("$\(pembelian)").font(.title).foregroundColor(.green)
                    Spacer()
                }
            
                Spacer(minLength: 50)
                Divider().padding(.bottom)
                Keyboard(pembelian: $pembelian)
                NavigationLink(
                    destination: DetailTransaction(buyPage: $buyPage, indeksCrypto: indeksCrypto, buy: pembelian),
                    label: {
                        Text("Buy").frame(width:300 ,height: 40, alignment: .center).background(Color.blue).foregroundColor(.white).cornerRadius(10).padding()
                    })
               
            }.offset(y:-50)
            .navigationBarItems(leading: Button(action: {
                presenter.wrappedValue.dismiss()
            }, label: {
                Image(systemName: "multiply.circle.fill").foregroundColor(Color.theme.utama)
            }))
        }
    
    }
}

//struct BuyPage_Previews: PreviewProvider {
//    static var previews: some View {
//        BuyPage()
//    }
//}
