//
//  PortofolioView.swift
//  Investwatch
//
//  Created by bevan christian on 22/06/21.
//

import SwiftUI

struct PortofolioView: View {
    //@EnvironmentObject var cryptoVm:PortofolioVM
    @EnvironmentObject var porto:PortofolioVM
    var body: some View {
        ZStack {
            Color.theme.background
            NavigationView{
                VStack(alignment:.leading) {
                    if porto.totalReturn > 0.0 {
                        Text("Return \(porto.totalReturn)%").font(.title3).multilineTextAlignment(.leading).foregroundColor(Color.green).padding(.leading)
                        HStack{
                            Text("Asset $\(String(format: "%.2f", porto.totalAset))").font(.headline).multilineTextAlignment(.leading).foregroundColor(Color.theme.kedua).padding(.leading)
                            Spacer()
                            Text("Gain $\(porto.total ?? 100.0)").font(.headline).multilineTextAlignment(.leading).foregroundColor(Color.theme.kedua).padding(.leading)
                        }.padding(.trailing)
                        
                    } else {
                        Text("Return \(porto.totalReturn)%").font(.title3).multilineTextAlignment(.leading).foregroundColor(Color.red).padding(.leading)
                        HStack{
                            Text("Asset $\(String(format: "%.2f", porto.totalAset))").font(.headline).multilineTextAlignment(.leading).foregroundColor(Color.theme.kedua).padding(.leading)
                            Spacer()
                            Text("Return $\(porto.total)").font(.headline).multilineTextAlignment(.leading).foregroundColor(Color.theme.kedua).padding(.leading)
                        }.padding(.trailing)
                    }
                    
                    Divider()
                    ScrollView{
                        VStack{
                            LazyVGrid(columns: [GridItem(.fixed(180)),GridItem(.fixed(180))], content: {
                                ForEach(porto.listPorto){ data in
                                    NavigationLink(
                                        destination: DetailPorto(coin: data),
                                        label: {
                                            PortofolioCard( data: data, crypto: porto).background(Color.theme.backgroundDua).cornerRadius(20)
                                        })
                                    
                                }
                            })
                            NavigationLink(destination: EmptyView()) {
                                EmptyView()
                            }
                            NavigationLink(destination: EmptyView()) {
                                EmptyView()
                            }
                            
                        }
                    }
                }
                
                .navigationTitle("Portofolio")
            }
        }.background(Color.theme.background.edgesIgnoringSafeArea(.all)).onAppear {
            porto.subsicreCrypto()
            porto.getPorto()
            porto.getTotal()
        }
    }
}

struct PortofolioView_Previews: PreviewProvider {
    static var previews: some View {
        PortofolioView()
    }
}
