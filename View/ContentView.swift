//
//  ContentView.swift
//  Investwatch
//
//  Created by bevan christian on 16/06/21.
//

import SwiftUI

struct ContentView: View {
    //@Environment(\.managedObjectContext) var managedObjectContext
    @EnvironmentObject var coredata:coreDataVm
    @EnvironmentObject var cryptoVm:RequestVM
    @State var datalist:[CryptoListModel]
    @State var favKosong = false
    @State var listKosong = true
    @State var onboarding = true
    @State var searchText:String
    var body: some View {
        ZStack {
            Color.theme.background
            NavigationView {
                if cryptoVm.listSaham.count == 0 {
                    LottieView(name: "loading", loopMode: .loop)
                        .frame(width: 350, height: 350)
                }else {
                    VStack(alignment: .leading){
                        Text("Watchlist").font(.title2)
                            .fontWeight(.semibold)
                            .multilineTextAlignment(.leading).padding(.leading)
                    
                            List{
                                ScrollView(.horizontal){
                                    HStack {
                                        ForEach(cryptoVm.listFavorit){ data in
                                            NavigationLink(destination: DetailCrypto(coin: data)) {
                                                CardHorizontal(crypto: data).listRowInsets(EdgeInsets())
                                            }
                                           
                                            
                                            
                                        }
                                    }
                                }
                            }.frame(height:180).listStyle(PlainListStyle())
                    
                        Text("Top Volume").font(.title2)
                            .fontWeight(.semibold)
                            .multilineTextAlignment(.leading).padding(.leading)
                        List{
                            ForEach(cryptoVm.listSaham){ data in
                                NavigationLink(
                                    destination: DetailCrypto(coin: data),
                                    label: {
                                        CardList(price: String(data.currentPrice!), nama: data.name!, volume: String(data.totalVolume!), image: data, cryptoVm: cryptoVm )
                                    }).frame(height:60)
                            }
                        }.listStyle(PlainListStyle())
                        
                    }
                    .navigationTitle("Your watch")
                }
                
                
            }
        }.onAppear {
            cryptoVm.subsicreCrypto()
            cryptoVm.subscribeSearch()
        }
    }
    
    struct ContentView_Previews: PreviewProvider {
        @StateObject static var modelData = RequestVM()
        static var previews: some View {
            ContentView(datalist: [CryptoListModel()], searchText: "").environmentObject(modelData)
        }
    }
}



//
//VStack(alignment: .center) {
//    GeometryReader { geo in
//        TextField("Search your Coin", text: $searchText)
//            .border(Color.theme.backgroundDua, width: 0.5)
//            .frame(width: geo.size.width, height: 110, alignment:.center)
//            .background(Color.theme.background).textFieldStyle(RoundedBorderTextFieldStyle())
//            .keyboardType(.numberPad).offset(y: -30)
//            .onChange(of: searchText, perform: { value in
//                cryptoVm.search(type: value)
//            })
//    }.padding()
//
//}.frame(height: 80)
