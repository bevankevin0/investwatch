//
//  HalamanUtama.swift
//  Investwatch
//
//  Created by bevan christian on 27/06/21.
//

import SwiftUI

struct HalamanUtama: View {
    @State var onboarding = UserDefaults.standard.bool(forKey: "onboarding")
    var body: some View {
        if !onboarding {
            OnboardingView(onboarding: $onboarding)
        } else {
            TabView {
                ContentView(datalist: [CryptoListModel(),CryptoListModel()], searchText: "").tabItem {  VStack {
                    Image(systemName: "list.dash").foregroundColor(Color.theme.utama)
                    Text("Market").foregroundColor(Color.theme.kedua)
                } }.tag(1)
                SearchView().tabItem {
                    VStack {
                        Image(systemName: "magnifyingglass").foregroundColor(Color.theme.utama)
                        Text("Search").foregroundColor(Color.theme.kedua)
                    }
                   
                    
                }.tag(2)
                PortofolioView().tabItem {  VStack {
                    Image(systemName: "cart").foregroundColor(Color.theme.utama)
                    Text("Portofolio").foregroundColor(Color.theme.kedua)
                }}.tag(3)
                
            }
        }
    }
}

struct HalamanUtama_Previews: PreviewProvider {
    static var previews: some View {
        HalamanUtama()
    }
}
