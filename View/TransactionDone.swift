//
//  TransactionDone.swift
//  Investwatch
//
//  Created by bevan christian on 24/06/21.
//

import SwiftUI
import Lottie
struct TransactionDone: View {
    @Environment(\.presentationMode) var presenter
    @Binding var buyPage:Bool
    var body: some View {
            VStack {
                Spacer()
                LottieView(name: "confirmDark", loopMode: .loop)
                    .frame(width: 250, height: 250)
                Text("Transaction Done").font(.title).bold()
                Text("Your coin is stored in portofolio you can check it").font(.body).padding(.top,10)
                Spacer()
                Button(action: {
                    buyPage = false
                    presenter.wrappedValue.dismiss()
                }, label: {
                    Text("Close").frame(width:350 ,height: 40, alignment: .center).background(Color.blue).foregroundColor(.white).cornerRadius(10).padding()
                })
                   
            }.offset(y:-50)
        
       
    }
}

//struct TransactionDone_Previews: PreviewProvider {
//    static var previews: some View {
//        TransactionDone()
//    }
//}
