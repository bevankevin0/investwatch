//
//  DetailCrypto.swift
//  Investwatch
//
//  Created by bevan christian on 18/06/21.
//
//high24H,low24H,priceChange24H,currentPrice,marketCap,totalVolume
import SwiftUI
import SwiftUICharts


struct DetailPorto: View {
    
    var data = [2.0,3.0,4.0,5.0,6.0,4.0,3.0,9.0]
    // untuk download
    @EnvironmentObject var cryptoVm:RequestVM
    @EnvironmentObject var porto:PortofolioVM
    @State var crypto:CryptoListModel
    @EnvironmentObject var cryptoDetailVM:DetailVM
    @State private var minimize = false
    @State private var buy = false
    @State private var sell = false
    var threeColumnGrid = [GridItem(.adaptive(minimum: 110, maximum: 210))]
    var twoColumnGrid = [GridItem(.fixed(CGFloat(200.0))),GridItem(.fixed(CGFloat(150.0)))]
    var cryptoIndex:Int{
        porto.listCrypto.firstIndex { a in
            a.id == crypto.id
        }!
    }
    init(coin:CryptoListModel) {
        self.crypto = coin
        //_cryptoDetailVM = StateObject(wrappedValue: DetailVM(idCrypto: coin.id!))
    }
    
    var body: some View {
        ScrollView {
            VStack(alignment:.leading) {
                HStack() {
                    Spacer()
                    LikeButton(crypto: $crypto, index: cryptoIndex)
                    if porto.listCrypto[cryptoIndex].imageData == nil {
                        Image("gmb1").resizable().clipShape(Circle()).frame(width: 50, height: 50, alignment: .center)
                    }else {
                        Image(uiImage:porto.listCrypto[cryptoIndex].imageData!).resizable().clipShape(Circle()).frame(width: 60, height: 60, alignment: .center)
                    }
                    
                    
                    
                }
                .padding(.trailing)
                LineChart(data: crypto.sparklineIn7D?.price ?? data, title: crypto.name ?? "DEFAULT", price: String(crypto.currentPrice!))
                VStack(alignment: .center) {
                    GeometryReader { geo in
                        HStack {
                            Button {
                                buy.toggle()
                            } label: {
                                Text("Buy More").frame(width:geo.size.width * 0.5,height: 40 ,alignment: .center)
                                    .background(Color.blue)
                                    .foregroundColor(.white).cornerRadius(10)
                            }
                            Button {
                                sell.toggle()
                            } label: {
                                Text("Sell").frame(width:geo.size.width * 0.5,height: 40 ,alignment: .center)
                                    .background(Color.red)
                                    .foregroundColor(.white).cornerRadius(10)
                            }
                            
                        }
                        
                    }
                    
                }.padding([.leading, .trailing]).padding(.bottom,40)
                HStack {
                    Text("Description").font(.headline).padding(.horizontal)
                    Spacer()
                    ButtonMinimize(minimize: $minimize).padding(.trailing)
                    
                }
                if minimize  && cryptoDetailVM.detailCrypto.welcomeDescription?.en != nil {
                    HStack(alignment:.top){
                        Text(cryptoDetailVM.detailCrypto.welcomeDescription?.en ?? "No Data").font(.body).multilineTextAlignment(.leading).padding([.top, .leading, .trailing,.bottom]).transition(AnyTransition.slide)
                    }.frame(height:900,alignment: .topLeading)
                } else {
                    HStack(alignment:.top){
                        Text(cryptoDetailVM.detailCrypto.welcomeDescription?.en ?? "No Data").font(.body).multilineTextAlignment(.leading).padding([.top, .leading, .trailing,.bottom]).transition(AnyTransition.slide)
                    }.frame(height:190,alignment: .topLeading)
                }
                
                Spacer()
                Text("Indicator").font(.headline).padding(.horizontal)
                VStack {
                    IndicatorHorizontal(namaGambar: "divide.circle.fill", namaIndikator: "Your AVG Price", valueIndikator: "$\(crypto.hargaRataRata!)")
                    IndicatorHorizontal(namaGambar: "bitcoinsign.circle.fill", namaIndikator: "Total Coin", valueIndikator: "\(crypto.jumlahCoin!) \(crypto.id!)")
                    IndicatorHorizontal(namaGambar: "dollarsign.square.fill", namaIndikator: "Total Asset", valueIndikator: "$\(crypto.jumlahDolar!)")
                    IndicatorHorizontal(namaGambar: "percent", namaIndikator: "Return", valueIndikator: "\(crypto.presentasePerubahan!)%")
                }
                
            }.padding(.bottom, 60).animation(.easeIn)
        }.onAppear(perform: {
            cryptoDetailVM.downloadDetail(idCrypto: crypto.id!)
            cryptoVm.getFav()
        }).sheet(isPresented: $buy, content: {
            BuyPage(buyPage: $buy, indeksCrypto: cryptoIndex)
        }).sheet(isPresented: $sell, content: {
            SellPage(crypto: crypto, sellPage: $sell, image: porto.listCrypto[cryptoIndex].imageData ?? UIImage(named: "gmb1"), indeksCrypto: cryptoIndex)
        })
    }
}





//struct DetailCrypto_Previews: PreviewProvider {
//    static var cryptoVm = RequestVM()
//    static var previews: some View {
//        DetailCrypto(crypto: cryptoVm.listSaham[0], cryptoVM: cryptoVm)
//    }
//}
