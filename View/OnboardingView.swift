//
//  OnboardingView.swift
//  Investwatch
//
//  Created by bevan christian on 26/06/21.
//

import SwiftUI

struct OnboardingView: View {
    @State var selection = 0
    @Binding var onboarding:Bool
    let tab = tabIsi()
    
    var body: some View {
        VStack(alignment: .trailing) {
            Button(action: {
                onboarding = true
                UserDefaults.standard.set(onboarding, forKey: "onboarding")
            }, label: {
                Text("Close").frame(width: 140, height: 44, alignment: .trailing)
            }).padding(.horizontal)
            TabView(selection:$selection) {
                ForEach(tab.tab) { num in
                    TabViewComponent(index: num.id)
                }
                
            }.accentColor(.orange).tabViewStyle(PageTabViewStyle())
            .indexViewStyle(PageIndexViewStyle(backgroundDisplayMode: .always))
            
        }
        
    }
}

struct OnboardingView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingView(onboarding: .constant(true))
    }
}
