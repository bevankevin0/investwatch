//
//  DetailCrypto.swift
//  Investwatch
//
//  Created by bevan christian on 18/06/21.
//
//high24H,low24H,priceChange24H,currentPrice,marketCap,totalVolume
import SwiftUI
import SwiftUICharts


struct DetailCrypto: View {
  
    var data = [2.0,3.0,4.0,5.0,6.0,4.0,3.0,9.0]
    // untuk download
    @EnvironmentObject var cryptoVm:RequestVM
    @State var crypto:CryptoListModel
    @EnvironmentObject var cryptoDetailVM:DetailVM
    @State private var minimize = false
    @State private var buy = false
    var threeColumnGrid = [GridItem(.adaptive(minimum: 110, maximum: 110))]
    var cryptoIndex:Int{
        cryptoVm.listSaham.firstIndex { a in
            a.id == crypto.id
        }!
    }
    init(coin:CryptoListModel) {
        self.crypto = coin
       // _cryptoDetailVM = StateObject(wrappedValue: DetailVM(idCrypto: coin.id!))
    }
    
    var body: some View {
        ScrollView {
            VStack(alignment:.leading) {
                HStack() {
                    Spacer()
                    LikeButton(crypto: $crypto, index: cryptoIndex)
                    if cryptoVm.listSaham[cryptoIndex].imageData == nil {
                        Image("gmb1").resizable().clipShape(Circle()).frame(width: 50, height: 50, alignment: .center)
                    }else {
                        Image(uiImage:cryptoVm.listSaham[cryptoIndex].imageData!).resizable().clipShape(Circle()).frame(width: 60, height: 60, alignment: .center)
                    }
                   
                    
                    
                }
                .padding(.trailing)
                LineChart(data: crypto.sparklineIn7D?.price ?? data, title: crypto.name ?? "DEFAULT", price: String(crypto.currentPrice!))
                VStack(alignment: .center) {
                    GeometryReader { geo in
                        
                        Button{
                            buy.toggle()
                        } label: {
                            Text("Buy")
                                .frame(width:geo.size.width,height: 40,alignment: .center)
                                .background(Color.red)
                                .foregroundColor(.white)
                                .cornerRadius(10)
                        }
                    }
                  
                }.padding([.leading, .trailing]).padding(.bottom,40)
                HStack {
                    Text("Description").font(.headline).padding(.horizontal)
                    Spacer()
                    ButtonMinimize(minimize: $minimize).padding(.trailing)
                }
            
                    if minimize  && cryptoDetailVM.detailCrypto.welcomeDescription?.en != nil {
                        HStack(alignment:.top){
                        Text(cryptoDetailVM.detailCrypto.welcomeDescription?.en ?? "No Data").font(.body).multilineTextAlignment(.leading).padding([.top, .leading, .trailing,.bottom]).transition(AnyTransition.slide)
                        }.frame(height:900,alignment: .topLeading)
                    } else {
                        HStack(alignment:.top){
                        Text(cryptoDetailVM.detailCrypto.welcomeDescription?.en ?? "No Data").font(.body).multilineTextAlignment(.leading).padding([.top, .leading, .trailing,.bottom]).transition(AnyTransition.slide)
                        }.frame(height:190,alignment: .topLeading)
                    }
             
              
                
                Spacer()
                Text("Indicator").bold().font(.headline).padding(.horizontal)
                LazyVGrid(columns: threeColumnGrid,alignment:.leading,spacing:10) {
                    IndicatorView(alignment: .leading, titleIndicator: "High 24H", indikator: "\(crypto.high24H!)")
                    IndicatorView(alignment: .leading, titleIndicator: "Low 24H", indikator: "\(crypto.low24H!)")
                    IndicatorView(alignment: .leading, titleIndicator: "Price Change 24H", indikator: "\(crypto.priceChange24H!)")
                    IndicatorView(alignment: .leading, titleIndicator: "Current Price", indikator: "\(crypto.currentPrice!)")
                    IndicatorView(alignment: .leading, titleIndicator: "Market Cap", indikator: "\(crypto.marketCap!)")
                    IndicatorView(alignment: .leading, titleIndicator: "Total Volume", indikator: "\(crypto.totalVolume!)")
                    IndicatorView(alignment: .leading, titleIndicator: "$ Change % Currency", indikator: "\(crypto.priceChangePercentage24HInCurrency!)")
                    IndicatorView(alignment: .leading, titleIndicator: "$ Change", indikator: "\(crypto.priceChange24H!)")
                    IndicatorView(alignment: .leading, titleIndicator: "$ Change %", indikator: "\(crypto.priceChangePercentage24H!)")
                    
                }
                .padding(.leading)
            }.padding(.bottom, 60).animation(.easeIn)
        }.onAppear(perform: {
            //cryptoVm.getDetail(id: crypto.id!)
            //cryptoDetailVM = DetailVM(idCrypto: crypto.id)
            cryptoDetailVM.downloadDetail(idCrypto: crypto.id!)
            cryptoVm.getFav()
        }).sheet(isPresented: $buy, content: {
            BuyPage(buyPage: $buy, indeksCrypto: cryptoIndex)
        })
    }
//    init(crypto:CryptoListModel) {
//        self.crypto = crypto
//        //self.cryptoVM = cryptoVM
//        self.cryptoDetailVM = DetailVM(namaCrypto:cryptoVM.listSaham[cryptoIndex].id!)
//        //self.imageManager = im
//    }
}





//struct DetailCrypto_Previews: PreviewProvider {
//    static var cryptoVm = RequestVM()
//    static var previews: some View {
//        DetailCrypto(crypto: cryptoVm.listSaham[0], cryptoVM: cryptoVm)
//    }
//}
