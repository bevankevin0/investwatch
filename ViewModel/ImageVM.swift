//
//  ImageVM.swift
//  Investwatch
//
//  Created by bevan christian on 21/06/21.
//

import Foundation
import SwiftUI
import Combine
class ImageVM:ObservableObject {
    @Published var image:UIImage? = nil
    private var imageService:ImageService
    private var requestVm = RequestVM()
    private var imageSubs = Set<AnyCancellable>()
   // var cryptoListModel:CryptoListModel?
    
    init(cryptoList:CryptoListModel) {
       // self.cryptoListModel = cryptoList
        imageService = ImageService(urlImage: cryptoList.image!)
        subsImageService()
    }
    
//    func subsGambar(){
//        requestVm.$listSaham.sink { completion in
//            print(completion)
//        } receiveValue: { [self] listSaham in
//            var cryptoIndex:Int{
//                listSaham.firstIndex { a in
//                    a.id == cryptoListModel?.id
//                } ?? 0
//            }
//            listSaham[cryptoIndex].imageData = image!
//        }
//
//    }
    
    
  
    
    func subsImageService(){
        imageService.$image.sink { completion in
            print(completion)
        } receiveValue: { [self] gambar in
            self.image = gambar
        }.store(in: &imageSubs)

    }
}

