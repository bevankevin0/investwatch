//
//  DetailVM.swift
//  Investwatch
//
//  Created by bevan christian on 19/06/21.
//

import Foundation
import Combine

class DetailVM: ObservableObject {
    @Published var detailCrypto = CryptoDetailModel()
    @Published var listSaham = [CryptoListModel]()
    private var cancelable = Set<AnyCancellable>()
    private var detailService:DetailService?
    //private var requestVm = RequestVM()
    
   func downloadDetail(idCrypto:String) {
        detailService = DetailService()
        detailService?.downloadDetail(idCrypto: idCrypto)
        subscribeDetail()
    }
    
     func subscribeDetail(){
        detailService!.$detailCrypto.sink {(completion) in
            print(completion)
        } receiveValue: { [weak self] (detail) in
            self?.detailCrypto = detail
        }.store(in: &cancelable)

    }
   
    
    
    
   
    
    
//    func getDetail(namaCrypto:String,completion:@escaping(CryptoDetailModel)->Void){
//        Bundle.main.request(urlString: linkPenting.urlDetail(namaCrypto).stringValue) { hasil, selesai in
//            DispatchQueue.main.async {
//                completion(hasil)
//            }
//            
//        }
//        
//    }
    
}
