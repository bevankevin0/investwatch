//
//  PortofolioVM.swift
//  Investwatch
//
//  Created by bevan christian on 26/06/21.
//

import Foundation
import Combine


class PortofolioVM:ObservableObject {
    @Published var listCrypto = [CryptoListModel]()
    private let requestVm = CryptoService()
    private var cancellables2 = Set<AnyCancellable>()
    let persistence = coreDataVm()
    @Published var listPorto = [CryptoListModel]()
    @Published var total = 0.0
    var listNamaPorto = [Portofolio]()
    @Published var jumlahKoin = "0"
    @Published var jumlahCrypto = "0"
    @Published var jumlahCryptoJual = "0"
    let cryptoVm = RequestVM()
    @Published var totalReturn = 0.0
    @Published var totalAset = 0.0
    
    
    
    
    func getTotal(){
        persistence.getTotal()
        if persistence.total.count > 0 {
            total = persistence.total[0].total
        }
       
        print("atas iki total")
    }
    
    func subsicreCrypto(){
        requestVm.$listSaham.sink { [weak self] (hasil) in
            self?.listCrypto = hasil
            print("anji")
            //self?.kontainer = hasil
            }.store(in: &cancellables2)
       
      
    }
    
    
    func getPorto(){
        listPorto.removeAll()
        persistence.getPortofolio()
        listNamaPorto = persistence.listPorto
       
       // print(listCrypto)
        print(listNamaPorto.count)
        sort()
    }
    
    
    
    func sort(){
        totalAset = 0.0
        totalReturn = 0.0
        // listcrypto itu dari requestvm isie semua crypto
        // listnamaporto itu isie dari core data
        // di cek kalo ada di core maka dimasukan list porto
        if listNamaPorto.count > 0 && listCrypto.count > 0 {
            for porto in 0...listCrypto.count-1 {
                for nama in 0...listNamaPorto.count-1 {
                    if listCrypto[porto].id == listNamaPorto[nama].id {
                        listCrypto[porto].hargaRataRata = listNamaPorto[nama].hargaAvg
                        listCrypto[porto].jumlahCoin = listNamaPorto[nama].jumlahCoin
                        listCrypto[porto].jual = listNamaPorto[nama].jual
                        listCrypto[porto].jumlahDolar = listNamaPorto[nama].jumlahDolar
                        let presentaseAtas =  Double(listCrypto[porto].currentPrice!) - Double(listNamaPorto[nama].hargaAvg!)!
                        let presentaseBawah = presentaseAtas / Double(listNamaPorto[nama].hargaAvg!)! * 100
                        listCrypto[porto].presentasePerubahan = String(presentaseBawah)
                        totalReturn += presentaseBawah
                        totalAset += Double(listNamaPorto[nama].jumlahDolar!)!
                        print(listNamaPorto[nama].jumlahDolar)
                        listPorto.append(listCrypto[porto])
                        print("masuk porto")
                    }else {
                        print("gamasuk")
                        continue
                    }
                }
            }
        }
        
    }
    
    
    
    
    
}
