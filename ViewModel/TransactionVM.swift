//
//  TransactionVM.swift
//  Investwatch
//
//  Created by bevan christian on 22/06/21.
//

import Foundation
import Combine

class TransactionVM: ObservableObject {
    @Published var listCrypto = [CryptoListModel]()
    @Published var listPorto = [CryptoListModel]()
    @Published var total = 0.0
    var listNamaPorto = [Portofolio]()
    @Published var jumlahKoin = "0"
    @Published var jumlahCrypto = "0"
    @Published var jumlahCryptoJual = "0"
    let persistence = coreDataVm()
    let cryptoVm = RequestVM()
    @Published var totalReturn = 0.0
    @Published var totalAset = 0.0
    private var cancellables = Set<AnyCancellable>()
    
    
    init() {
        subsList()
        getPorto()
        
    }
    
    func getTotal(){
        persistence.getTotal()
        if persistence.total.count > 0 {
            total = persistence.total[0].total
        }
       
    }
    
    func subsList(){
//        if listCrypto.count == 0 {
//            cryptoVm.subsicreCrypto()
//        }
        cryptoVm.$listSaham.sink { listcrypto in
            self.listCrypto = listcrypto
//            print(listcrypto)
//            print("dapet sudahan list")
        }.store(in: &cancellables)
    }
    
    func getPorto(){
        listPorto.removeAll()
        persistence.getPortofolio()
        listNamaPorto = persistence.listPorto
        print("anji")
        print(listNamaPorto.count)
        sort()
    }
    
    func sort(){
        totalAset = 0.0
        totalReturn = 0.0
        // listcrypto itu dari requestvm isie semua crypto
        // listnamaporto itu isie dari core data
        // di cek kalo ada di core maka dimasukan list porto
        if listNamaPorto.count > 0 && listCrypto.count > 0 {
            for porto in 0...listCrypto.count-1 {
                for nama in 0...listNamaPorto.count-1 {
                    if listCrypto[porto].id == listNamaPorto[nama].id {
                        listCrypto[porto].hargaRataRata = listNamaPorto[nama].hargaAvg
                        listCrypto[porto].jumlahCoin = listNamaPorto[nama].jumlahCoin
                        listCrypto[porto].jual = listNamaPorto[nama].jual
                        listCrypto[porto].jumlahDolar = listNamaPorto[nama].jumlahDolar
                        let presentaseAtas =  Double(listCrypto[porto].currentPrice!) - Double(listNamaPorto[nama].hargaAvg!)!
                        let presentaseBawah = presentaseAtas / Double(listNamaPorto[nama].hargaAvg!)!
                        listCrypto[porto].presentasePerubahan = String(presentaseBawah)
                        totalReturn += presentaseBawah
                        totalAset += Double(listNamaPorto[nama].jumlahDolar!)!
                        print(listNamaPorto[nama].jumlahDolar)
                        listPorto.append(listCrypto[porto])
                        print("masuk porto")
                    }else {
                        print("gamasuk")
                        continue
                    }
                }
            }
        }
        
    }
    
    func totalPorto(){
        let jumlahPorto = listPorto.count
        totalAset = totalAset
        totalReturn = totalReturn/Double(jumlahPorto)
    }
    
    
    
    func kalkulasi(hargaNow:String,jumlahBeli:String){
        let doubleSementara = Double(jumlahBeli)!/Double(hargaNow)!
        jumlahCrypto = String(doubleSementara)
        
    }
    
    func kalkulasiJual(hargaNow:String,jumlahJual:String){
        let doubleSementara = Double(jumlahJual)!/Double(hargaNow)!
        jumlahCryptoJual = String(doubleSementara)
        
    }
    
    func buy(hargaNow:String,jumlahBeli:String,idCrypto:String,namaCrypto:String){
        // cek di core data ada yang id sama ga
        persistence.getPortoFiltered(id: idCrypto, cryptoVm: cryptoVm)
        // kalo ada update kalo ga ada create new
        if persistence.listPorto.count != 0 {
            print("crypto sama")
            let jumlahKoinDouble = Double(persistence.listPorto[0].jumlahCoin!)! + Double(jumlahCrypto)!
            persistence.listPorto[0].jumlahCoin! = String(jumlahKoinDouble)
            
            
            let totalAset = Double(persistence.listPorto[0].jumlahDolar!)! + Double(jumlahBeli)!
            let avgCost = totalAset / Double(persistence.listPorto[0].jumlahCoin!)!
            persistence.listPorto[0].hargaAvg = String(avgCost)
            
            let dolar = Double(persistence.listPorto[0].jumlahDolar!)! + Double(jumlahBeli)!
            persistence.listPorto[0].jumlahDolar = String(dolar)
            
            persistence.save()
            getPorto()
            
            print("update")
        } else {
            let avg = Double(jumlahBeli)! / Double(jumlahCrypto)!
            persistence.savePortofolio(nama: namaCrypto, id: idCrypto, jual: false, jumlahDolar: jumlahBeli, hargaAvg: String(avg), jumlahCoin: jumlahCrypto)
            print("save")
            getPorto()
        }
    }
    
    
    func sell(hargaNow:String,jumlahJual:String,idCrypto:String,namaCrypto:String,presentase:String){
        // cek di core data ada yang id sama ga
        persistence.getTotal()
        persistence.getPortoFiltered(id: idCrypto, cryptoVm: cryptoVm)
        // kalo ada update kalo ga ada create new
        if persistence.listPorto.count != 0 {
            print("crypto sama jual")
            let jumlahKoinDouble = Double(persistence.listPorto[0].jumlahCoin!)! - Double(jumlahCryptoJual)!
            persistence.listPorto[0].jumlahCoin! = String(jumlahKoinDouble)
            let jumlahJualDouble = Double(persistence.listPorto[0].jumlahDolar!)! - Double(jumlahJual)!
            persistence.listPorto[0].jumlahDolar = String(jumlahJualDouble)
            
            
            // ketika jual tidak mengganti hrga rata"
            //                 let totalAset = Double(persistence.listPorto[0].jumlahDolar!)! - Double(jumlahJual)!
            //                 let avgCost = totalAset / Double(persistence.listPorto[0].jumlahCoin!)!
            //                 persistence.listPorto[0].hargaAvg = String(avgCost)
            
//            Profit (P) = ( (SP * NS) - SC ) - ( (BP * NS) + BC )
//            Where:
//            NS is the number of shares,
//            SP is the selling price per share,
//            BP is the buying price per share,
//            SC is the selling commission,
//            BC is the buying commission.
//            https://goodcalculators.com/stock-calculator/
//            © 2015-2021 goodcalculators.com
            let hargaJual = Double(hargaNow)! - Double(persistence.listPorto[0].hargaAvg!)!
           // let hargaBeli =  * Double(jumlahCryptoJual)!
            let capitalGain = hargaJual * Double(jumlahCryptoJual)!
            print("bawah jumla dolar")
            print(persistence.listPorto[0].jumlahDolar!)
            print("harga jual \(hargaNow) harga now \(hargaNow) dikurangi average \(persistence.listPorto[0].hargaAvg!)")
            print(capitalGain)
            let dolar = Double(persistence.listPorto[0].jumlahDolar!)! - Double(jumlahJual)!
            if dolar <= 0 {
                persistence.listPorto[0].jual = true
                persistence.listPorto[0].hargaAvg = "0"
                persistence.listPorto[0].jumlahDolar = "0"
                persistence.listPorto[0].jumlahCoin = "0"
            }
            
            if persistence.total.count == 0 {
                print("total save")
                persistence.saveTotal(jumlah: capitalGain)
            } else {
                if capitalGain < 0 {
                    print("total update")
                    persistence.total[0].total += capitalGain
                } else {
                    print("total update")
                    persistence.total[0].total += capitalGain
                }
               
            }
            persistence.save()
            getPorto()
            getTotal()
            print("update jual")
        } else {
            print("error tidka terbaca")
        }
        
        
        
    }
}
