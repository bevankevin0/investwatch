//
//  coreDataVm.swift
//  Investwatch
//
//  Created by bevan christian on 20/06/21.
//

import Foundation

class coreDataVm:ObservableObject {
    @Published var listFavorit = [CryptoListModel]()
    @Published var total = [Total]()
    var listNamaFavorit = [Favorit]()
    @Published var listPorto = [Portofolio]()
    let persistence = PersistenceControler.shared
    
    func delete(id:String){
        if let yangDidelete = listNamaFavorit.first(where: { datafav in datafav.id == id}){
            
            persistence.delete(yangDidelete) { s in
                print(s)
            }
        }
    }
    
    func getPortofolio(){
        persistence.getPorto { porto in
            self.listPorto = porto
            
        }
       
    }
    
    func savePortofolio(nama:String,id:String,jual:Bool,jumlahDolar:String,hargaAvg:String,jumlahCoin:String){
        let porto = Portofolio(context: persistence.container.viewContext)
        porto.hargaAvg = hargaAvg
        porto.id = id
        porto.jual = jual
        porto.jumlahDolar = jumlahDolar
        porto.jumlahCoin = jumlahCoin
        porto.nama = nama
        persistence.save()
    }
    
    func saveTotal(jumlah:Double){
        let total = Total(context: persistence.container.viewContext)
        total.total = jumlah
        persistence.save()
    }
    func save(){
        persistence.save()
    }
    
    
    func getPortoFiltered(id:String,cryptoVm:RequestVM){
        persistence.getPortoSelected(idCrypto: id) { porto in
            self.listPorto = porto
        }
    }
    
    func getTotal(){
        persistence.getTotal { total in
            self.total = total
            print("total")
            print(total)
        }
    }
    
    
    func getFav(cryptoVm:RequestVM) {
        listFavorit.removeAll()
        persistence.getFavorit { [self] hasil in
            listNamaFavorit = hasil
            sort(cryptoVm: cryptoVm)
        }
    }
    
    func saveFavorit(id:String,nama:String){
        let fav = Favorit(context: persistence.container.viewContext)
        fav.id = id
        fav.nama = nama
        persistence.save()
    }
    
    func sort(cryptoVm:RequestVM) {
        if cryptoVm.listSaham.count != 0 {
            for crypto in 0...cryptoVm.listSaham.count-1 {
                if let ketemu = listNamaFavorit.first(where: { nama in nama.nama == cryptoVm.listSaham[crypto].name}) {
                    listFavorit.append(cryptoVm.listSaham[crypto])
                }
            }
        }
    }
}
