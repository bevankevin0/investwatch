//
//  RequestVM.swift
//  Investwatch
//
//  Created by bevan christian on 16/06/21.
//

import Foundation
import SwiftUI
import Combine


class RequestVM: ObservableObject {
    @Published var listSaham = [CryptoListModel]()
    @Published var listFavorit = [CryptoListModel]()
    @Published var kontainer = [CryptoListModel]()
    @Published var searchText = ""
    private let requestVm = CryptoService()
    private var cancellables = Set<AnyCancellable>()
    private var cancellables2 = Set<AnyCancellable>()

    deinit {
            cancellables2.removeAll()
           print("\(self): \(#function)")
       }
    
    
    func subscribeSearch(){
        $searchText
            .debounce(for: .seconds(0.5), scheduler: DispatchQueue.main)// untuk delay biar ga langsung masuk
            .tryMap { hasil -> String? in
                let hasil2 = hasil.lowercased()
                return hasil2
            }.sink { completion in
                print(completion)
            } receiveValue: { [weak self] search in
                self?.search(type: search)
            }.store(in: &cancellables)
        
    }
    
    func subsicreCrypto(){
        requestVm.$listSaham
            .sink { [weak self] (hasil) in
            self?.listSaham = hasil
            //self?.kontainer = hasil
            self?.getFav()
            }.store(in: &cancellables2)
       
      
    }
    
    func search(type:String?){
        let filtered = listSaham.filter { saham in
            (saham.name?.lowercased().contains(type ?? ""))!
        }
        kontainer = filtered
        //        if filtered.count == 0 {
        //            listSaham = kontainer
        //        }else {
        //            listSaham = filtered
        //        }
    }
    
    
    
    func geturutan(hasi:CryptoDetailModel){
        var cryptoIndex:Int{
            listSaham.firstIndex { a in
                a.id == hasi.id
            } ?? 0
        }
        
        listSaham[cryptoIndex].deskripsi = hasi.welcomeDescription?.en
    }
    
    
    
    
    
    func getImage(){
        if listSaham.count != 0 {
            for crypto in 0...listSaham.count-1 {
                guard let url =  URL(string: listSaham[crypto].image!) else {
                    print("gambar tidak ada")
                    return
                }
                let request = URLRequest(url: url)
                URLSession.shared.dataTask(with: request) { Data, response, error in
                    guard error == nil else { return }
                    if let datafix = Data {
                        DispatchQueue.main.async {
                            self.listSaham[crypto].imageData = UIImage(data: datafix)
                        }
                        
                    }
                }.resume()
            }
            
        }
        
    }
    
    
    
    /// core data
    
    var listNamaFavorit = [Favorit]()
    let persistence = PersistenceControler.shared
    
    func delete(id:String){
        if let yangDidelete = listNamaFavorit.first(where: { datafav in datafav.id == id}){
            
            persistence.delete(yangDidelete) { s in
                print(s)
            }
        }
        
        
        
    }
    
    
    func getFav() {
        listFavorit.removeAll()
        persistence.getFavorit { [self] hasil in
            listNamaFavorit = hasil
            sort()
        }
    }
    
    func saveFavorit(id:String,nama:String){
        let fav = Favorit(context: persistence.container.viewContext)
        fav.id = id
        fav.nama = nama
        persistence.save()
    }
    
    func sort() {
        if self.listSaham.count != 0 {
            for crypto in 0...self.listSaham.count-1 {
                if let ketemu = listNamaFavorit.first(where: { nama in nama.nama == self.listSaham[crypto].name}) {
                    // true soale dia fav
                    self.listSaham[crypto].favourite = true
                    listFavorit.append(self.listSaham[crypto])
                }
            }
        }
    }
    
    
}

















//
//
//
//
//
//func getData(urlString:String,completion:@escaping ([CryptoListModel])->Void){
//    guard let url = URL(string: urlString) else {
//        fatalError()
//    }
//    let request = URLRequest(url: url)
//    URLSession.shared.dataTask(with: request) { Data, response, error in
//        guard error == nil else { return }
//        let decoder = JSONDecoder()
//        if let hasilfix = try? decoder.decode([CryptoListModel].self, from: Data!) {
//            completion(hasilfix)
//        }else {
//           print("rusak")
//        }
//    }.resume()
//}
