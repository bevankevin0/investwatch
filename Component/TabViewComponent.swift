//
//  TabViewComponent.swift
//  Investwatch
//
//  Created by bevan christian on 27/06/21.
//

import SwiftUI
import Lottie

struct TabViewComponent: View {
    var index:Int
    var tab = tabIsi()
    var body: some View {
        ZStack {
            Color.theme.background
            VStack {
                Image("\(tab.tab[index].namaGambar)").resizable().aspectRatio(contentMode: .fit).frame(width: 180, height: 200)
                Text("\(tab.tab[index].headline)").font(.title).offset(y: -10)
                Text("\(tab.tab[index].caption)").font(.body).fontWeight(.light).multilineTextAlignment(.center).offset(y: -10)
            }.padding()
        }
    }
}

struct TabViewComponent_Previews: PreviewProvider {
    static var previews: some View {
        TabViewComponent(index: 1)
    }
}
