//
//  PortofolioCard.swift
//  Investwatch
//
//  Created by bevan christian on 23/06/21.
//

import SwiftUI

struct PortofolioCard: View {
    @State var data:CryptoListModel
    @StateObject var vm:ImageVM
    @State var cryptoVm:PortofolioVM
    var cryptoIndex:Int{
        cryptoVm.listCrypto.firstIndex { a in
            a.id == data.id
        }!
    }
    
    init(data:CryptoListModel,crypto:PortofolioVM) {
        self.data = data
        self.cryptoVm = crypto
        _vm = StateObject(wrappedValue: ImageVM(cryptoList: data))
    }
    var body: some View {
        HStack {
            VStack(alignment:.center) {
                if vm.image != nil {
                    Image(uiImage: vm.image!).resizable().clipShape(Circle()).frame(width: 80, height: 80, alignment: .leading)
                }else {
                    Image("gmb1").resizable().clipShape(Circle()).frame(width: 80, height: 80, alignment: .leading)
                }
               
                Text("\(data.name!)").foregroundColor(Color.theme.utama).font(.body).bold()
                HStack {
                    Text("Your Asset:")
                    Text(String(format: "%.2f", Double(data.jumlahDolar!)!))
                }.foregroundColor(Color.theme.kedua).font(.caption)
                HStack {
                    Text("Return:")
                    Text(String(format: "%.6f", Double(data.presentasePerubahan!)!))
                }.foregroundColor(Color.theme.kedua).font(.caption2)
            }
        }.frame(width: 180, height: 180, alignment: .center).ignoresSafeArea(.all).onAppear(perform: {
            cryptoVm.subsicreCrypto()
        }).onDisappear(perform: {
            cryptoVm.listCrypto[cryptoIndex].imageData = vm.image
            
        })
    }
}

//struct PortofolioCard_Previews: PreviewProvider {
//    static var previews: some View {
//        PortofolioCard()
//    }
//}
