//
//  CardList.swift
//  Investwatch
//
//  Created by bevan christian on 16/06/21.
//

import SwiftUI


struct CardList: View {
    var cryptoVm:RequestVM
    @StateObject var vm:ImageVM
    @State var price: String
    @State var nama: String
    @State var volume: String
    @State var image: CryptoListModel
    @State var imageData:Data?
    @State var imageUi:UIImage?
    var cryptoIndex:Int{
        cryptoVm.listSaham.firstIndex { a in
            a.id == image.id
        }!
    }
    
    init(price:String,nama:String,volume:String,image:CryptoListModel,cryptoVm:RequestVM) {
        self.price = price
        self.nama = nama
        self.volume = volume
        self.image = image
        self.cryptoVm = cryptoVm
        _vm = StateObject(wrappedValue: ImageVM(cryptoList: image))
    }
    
    var body: some View {
        VStack {
            HStack(alignment:.center) {
                if vm.image != nil {
                    Image(uiImage: vm.image!)
                        .resizable()
                        .scaledToFit()
                        .frame(width: 40, height: 50, alignment: .center)
                } else {
                    Image("gmb1").resizable()
                        .scaledToFit()
                        .frame(width: 40, height: 50, alignment: .center)
                }
                VStack(alignment:.leading) {
                    Text("\(nama)").foregroundColor(Color.theme.utama).bold()
                    Text("Volume : \(volume)").font(.caption).foregroundColor(Color.theme.kedua)
                }
                Spacer()
                VStack(alignment:.trailing) {
                    Text("Price").foregroundColor(Color.theme.utama)
                    Text("\(price)").font(.caption).foregroundColor(Color.theme.kedua).bold()
                }
            }
        }.padding([.leading, .bottom, .trailing]).onDisappear(perform: {
            cryptoVm.listSaham[cryptoIndex].imageData = vm.image
            // ketika pindah halaman di isi ke model biar nanti di detail gausa download
        })
    }
}




//struct CardList_Previews: PreviewProvider {
//    static var previews: some View {
//        CardList(price: "11", nama: "helow", marketCap: "120", image: "ddd")
//    }
//}
