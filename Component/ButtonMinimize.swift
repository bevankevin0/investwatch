//
//  ButtonMinimize.swift
//  Investwatch
//
//  Created by bevan christian on 19/06/21.
//

import SwiftUI

struct ButtonMinimize: View {
    @State var puter = false
    @Binding var minimize:Bool
    var body: some View {
        Button(action: {
            puter.toggle()
            minimize.toggle()
        }, label: {
            Image(systemName: "arrow.up.circle").foregroundColor(Color.theme.utama)
        }).rotationEffect(.degrees(puter ? 180 : 0))
    }
}

struct ButtonMinimize_Previews: PreviewProvider {
    @State static var kemin = false
    static var previews: some View {
        ButtonMinimize(minimize: $kemin)
    }
}
