//
//  IndicatorHorizontal.swift
//  Investwatch
//
//  Created by bevan christian on 26/06/21.
//

import SwiftUI

struct IndicatorHorizontal: View {
    var namaGambar:String
    var namaIndikator:String
    var valueIndikator:String
    var body: some View {
        VStack {
            HStack {
                Image(systemName: "\(namaGambar)").foregroundColor(Color.blue)
                Text("\(namaIndikator)").foregroundColor(Color.theme.utama)
                Spacer()
                Text("\(valueIndikator)").foregroundColor(Color.theme.kedua)
            }.padding()
            Divider()
        }
    }
}

//struct IndicatorHorizontal_Previews: PreviewProvider {
//    static var previews: some View {
//        IndicatorHorizontal()
//    }
//}
