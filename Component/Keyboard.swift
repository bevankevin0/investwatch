//
//  Keyboard.swift
//  Investwatch
//
//  Created by bevan christian on 22/06/21.
//

import SwiftUI

struct Keyboard: View {
    @Binding var pembelian:String
    var body: some View {
        LazyVGrid(columns: [GridItem(.fixed(100)),GridItem(.fixed(100)),GridItem(.fixed(100))], alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 50, pinnedViews: /*@START_MENU_TOKEN@*/[]/*@END_MENU_TOKEN@*/, content: {
            ForEach(1...9,id:\.self){ num in
                Button("\(num)") {
                    pembelian += String(num)
                }
            }
            Button(".") {
                pembelian += "."
            }
            Button("0") {
                pembelian += "0"
            }
            Button("<-") {
                if pembelian.count > 0 {
                    pembelian.removeLast()
                } else {
                    pembelian = ""
                }
              
            }
        }).font(.title2).foregroundColor(Color.theme.utama)
    }
}

//struct Keyboard_Previews: PreviewProvider {
//    static var previews: some View {
//        Keyboard(pembelian: <#Binding<String>#>)
//    }
//}
