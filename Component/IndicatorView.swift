//
//  IndicatorView.swift
//  Investwatch
//
//  Created by bevan christian on 19/06/21.
//

import Foundation
import SwiftUI

struct IndicatorView:View {
    var alignment:HorizontalAlignment
    var titleIndicator:String
    var indikator:String
    var body: some View {
        VStack(alignment:alignment){
            Text(titleIndicator)
                .font(.headline)
                .multilineTextAlignment(.leading)
            Text(indikator)
                .font(.caption).foregroundColor(Color.theme.kedua).multilineTextAlignment(.leading)
        }
    }
}
