//
//  LikeButton.swift
//  Investwatch
//
//  Created by bevan christian on 19/06/21.
//

import SwiftUI

struct LikeButton: View {
    //@Environment(\.managedObjectContext) var managedObjectContext
    @State private var like = true
    @Binding var crypto:CryptoListModel
    @EnvironmentObject var cryptoVm:RequestVM
    @EnvironmentObject var coredata:coreDataVm
    @State var index:Int
    var body: some View {
        Button(action: {
            cryptoVm.listSaham[index].favourite.toggle()
            //cryptoVm.getFav()
            if  cryptoVm.listSaham[index].favourite {
                cryptoVm.saveFavorit(id: cryptoVm.listSaham[index].id!, nama: cryptoVm.listSaham[index].name!)
                cryptoVm.getFav()
            } else {
                cryptoVm.delete(id: cryptoVm.listSaham[index].id!)
                cryptoVm.getFav()
                
            }
          
        }, label: {
            withAnimation {
                Image(systemName: cryptoVm.listSaham[index].favourite ?? true ? "heart.fill": "heart").foregroundColor(Color.red).scaleEffect(cryptoVm.listSaham[index].favourite ?? true ? 1.4:1.0)
            }
        })
    }
}

//struct LikeButton_Previews: PreviewProvider {
//    static var previews: some View {
//        LikeButton(crypto: CryptoListModel())
//    }
//}
