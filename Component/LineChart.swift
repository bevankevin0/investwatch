//
//  LineChart.swift
//  Investwatch
//
//  Created by bevan christian on 19/06/21.
//

import SwiftUI
import SwiftUICharts

struct LineChart: View {
    var data:[Double]
    var title:String
    var price:String
    var body: some View {
        SwiftUICharts.LineView(data: data, title:title, legend: "Current Price \(price)", style: .init(backgroundColor: Color.theme.background, accentColor: Color.theme.utama, gradientColor: GradientColor(start: Color.theme.utama, end: Color.theme.utama), textColor: Color.theme.kedua, legendTextColor: Color.theme.kedua, dropShadowColor: .gray)).padding(.horizontal).frame(height:310).offset(y: -60)
    }
}

//struct LineChart_Previews: PreviewProvider {
//    static var previews: some View {
//        LineChart()
//    }
//}
