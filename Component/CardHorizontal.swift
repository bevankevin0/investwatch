//
//  CardHorizontal.swift
//  Investwatch
//
//  Created by bevan christian on 18/06/21.
//

import SwiftUI

struct CardHorizontal: View {
    @EnvironmentObject var coredata:coreDataVm
    @EnvironmentObject var cryptoVm:RequestVM
    @StateObject var vm:ImageVM
    var crypto:CryptoListModel
    var indeks:Int {
        cryptoVm.listSaham.firstIndex { lstfav in
            lstfav.name == crypto.name
        }!
    }
    
    init(crypto:CryptoListModel) {
        _vm = StateObject(wrappedValue: ImageVM(cryptoList: crypto))
        self.crypto = crypto
    }
    var body: some View {
        HStack {
            VStack(alignment:.center) {
                if cryptoVm.listSaham[indeks].imageData != nil {
                    Image(uiImage: cryptoVm.listSaham[indeks].imageData!).resizable().clipShape(Circle()).frame(width: 80, height: 80, alignment: .leading)
                } else if vm.image != nil  {
                    Image(uiImage: vm.image!).resizable().clipShape(Circle()).frame(width: 80, height: 80, alignment: .leading)
                } else {
                    Image("gmb1").resizable().clipShape(Circle()).frame(width: 80, height: 80, alignment: .leading)
                }
              
                Text("\(cryptoVm.listSaham[indeks].name!)").foregroundColor(Color.theme.utama).font(.body).bold()
                    HStack {
                        Text("Price:")
                        Text(String(format: "%.3f", cryptoVm.listSaham[indeks].currentPrice!))
                    }.foregroundColor(Color.theme.kedua).font(.caption)
            }
        }.frame(width: 180, height: 180, alignment: .center).ignoresSafeArea(.all)
    }
}

//struct CardHorizontal_Previews: PreviewProvider {
//    static var previews: some View {
//        CardHorizontal()
//    }
//}
