//
//  Persistence.swift
//  Investwatch
//
//  Created by bevan christian on 20/06/21.
//

import Foundation
import CoreData
//@FetchRequest(entity: Favorit.entity(), sortDescriptors: []) var favorit : FetchedResults<Favorit>

struct PersistenceControler {
    static let shared = PersistenceControler() // ini singgleton
    
    let container:NSPersistentContainer
    
    init() {
        // hubungin ke erd
        container = NSPersistentContainer(name: "ERD")
        container.loadPersistentStores { descrption, error in
            if let error = error {
                fatalError("error core")
            }
        }
    }
    
    
    func save(completion:@escaping (Error?)->() = {_ in}){
        let context = container.viewContext
        do {
            try context.save()
            completion(nil)
        } catch  {
            completion(error) // kalo ada eror bawa eror e
        }
    }
    
    func getFavorit(completion:@escaping([Favorit])->Void){
        let request:NSFetchRequest<Favorit> = Favorit.fetchRequest()
        let context = container.viewContext
        do {
            var hasil = try context.fetch(request)
            completion(hasil)
        } catch  {
            print("favorit error")
        }
       
    }
    
    func getTotal(completion:@escaping([Total])->Void){
        let request:NSFetchRequest<Total> = Total.fetchRequest()
        let context = container.viewContext
        do {
            var hasil = try context.fetch(request)
            completion(hasil)
        } catch  {
            print("portofolio error")
        }
       
    }
    
    func getPorto(completion:@escaping([Portofolio])->Void){
        let request:NSFetchRequest<Portofolio> = Portofolio.fetchRequest()
        request.predicate = NSPredicate(format: "jumlahDolar > 0")
        let context = container.viewContext
        do {
            var hasil = try context.fetch(request)
            completion(hasil)
        } catch  {
            print("portofolio error")
        }
       
    }
    func getPortoSelected(idCrypto:String,completion:@escaping([Portofolio])->Void){
        let request:NSFetchRequest<Portofolio> = Portofolio.fetchRequest()
        request.predicate = NSPredicate(format: "id == %@", "\(idCrypto)")
        
        let context = container.viewContext
        do {
            var hasil = try context.fetch(request)
            completion(hasil)
        } catch  {
            print("portofolio filter error")
        }
       
    }
    
    
    func delete(_ object: NSManagedObject,completion:@escaping (Error?)->Void) {
        let context = container.viewContext
        context.delete(object)
        save(completion: completion)
    }
    
}
