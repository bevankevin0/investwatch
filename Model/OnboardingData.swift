//
//  OnboardingData.swift
//  Investwatch
//
//  Created by bevan christian on 27/06/21.
//

import Foundation

struct OnboardingData:Identifiable {
    var id:Int
    var namaGambar:String
    var headline:String
    var caption:String
}


struct tabIsi {
    let tab = [OnboardingData(id: 0, namaGambar: "onboard0", headline: "No Risk", caption: "With this application you can buy cryptocurrencies without worry"),OnboardingData(id: 1, namaGambar: "onboard1", headline: "Real Time", caption: "Data provided real time and accurate data Powered by CoinGecko")]
}
