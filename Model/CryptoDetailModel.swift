//
//  CryptoDetailModel.swift
//  Investwatch
//
//  Created by bevan christian on 19/06/21.
//

import Foundation
struct CryptoDetailModel: Codable {
    var id, symbol, name: String?
    var assetPlatformID: String?
    var platforms: [String:String]?
    var blockTimeInMinutes: Double?
    var hashingAlgorithm: String?
    var categories: [String]?
//    var publicNotice: String?
   // var additionalNotices: [String]?
    var welcomeDescription: Description?
    var links: Links?
    var image: image?
    var countryOrigin, genesisDate: String?
    var sentimentVotesUpPercentage, sentimentVotesDownPercentage: Double?
    var icoData: IcoData?
    var marketCapRank, coingeckoRank: Double?
    var coingeckoScore, developerScore, communityScore, liquidityScore: Double?
    var publicInterestScore: Double?
    var communityData: CommunityData?
    var publicInterestStats: PublicInterestStats?
    //var statusUpdates: [String]?
    var lastUpdated: String?
    

    enum CodingKeys: String, CodingKey {
        case id, symbol, name
        case assetPlatformID = "asset_platform_id"
        case platforms
        case blockTimeInMinutes = "block_time_in_minutes"
        case hashingAlgorithm = "hashing_algorithm"
        case categories
        //case publicNotice = "public_notice"
       // case additionalNotices = "additional_notices"
        case welcomeDescription = "description"
        case links, image
        case countryOrigin = "country_origin"
        case genesisDate = "genesis_date"
        case sentimentVotesUpPercentage = "sentiment_votes_up_percentage"
        case sentimentVotesDownPercentage = "sentiment_votes_down_percentage"
        case icoData = "ico_data"
        case marketCapRank = "market_cap_rank"
        case coingeckoRank = "coingecko_rank"
        case coingeckoScore = "coingecko_score"
        case developerScore = "developer_score"
        case communityScore = "community_score"
        case liquidityScore = "liquidity_score"
        case publicInterestScore = "public_interest_score"
        case communityData = "community_data"
        case publicInterestStats = "public_interest_stats"
       // case statusUpdates = "status_updates"
        case lastUpdated = "last_updated"
    }
}

// MARK: - CommunityData
struct CommunityData: Codable {
    var facebookLikes: Double?
    var twitterFollowers: Double?
    var redditAveragePosts48H, redditAverageComments48H: Double?
    var redditSubscribers, redditAccountsActive48H: Double?
    var telegramChannelUserCount: Double?

    enum CodingKeys: String, CodingKey {
        case facebookLikes = "facebook_likes"
        case twitterFollowers = "twitter_followers"
        case redditAveragePosts48H = "reddit_average_posts_48h"
        case redditAverageComments48H = "reddit_average_comments_48h"
        case redditSubscribers = "reddit_subscribers"
        case redditAccountsActive48H = "reddit_accounts_active_48h"
        case telegramChannelUserCount = "telegram_channel_user_count"
    }
}

// MARK: - IcoData
struct IcoData: Codable {
    var icoStartDate, icoEndDate, shortDesc: String?
    //var icoDataDescription: String?
    var softcapCurrency, hardcapCurrency, totalRaisedCurrency: String?
  //  var softcapAmount, hardcapAmount, totalRaised: JSONNull?
    var quotePreSaleCurrency: String?
    //var basePreSaleAmount, quotePreSaleAmount: JSONNull?
    var quotePublicSaleCurrency: String?
    var linksdalam:gatau?
    var basePublicSaleAmount: Double?
    var quotePublicSaleAmount: Double?
    var acceptingCurrencies, countryOrigin: String?
   // var preSaleStartDate, preSaleEndDate: JSONNull?
    var whitelistURL: String?
   // var whitelistStartDate, whitelistEndDate: JSONNull?
    var bountyDetailURL: String?
    //var amountForSale: JSONNull?
    var kycRequired: Bool?
   // var whitelistAvailable, preSaleAvailable: JSONNull?
    var preSaleEnded: Bool?

    enum CodingKeys: String, CodingKey {
        case icoStartDate = "ico_start_date"
        case icoEndDate = "ico_end_date"
        case shortDesc = "short_desc"
        case linksdalam = "links"
       // case icoDataDescription = "description"
        case softcapCurrency = "softcap_currency"
        case hardcapCurrency = "hardcap_currency"
        case totalRaisedCurrency = "total_raised_currency"
//        case softcapAmount = "softcap_amount"
//        case hardcapAmount = "hardcap_amount"
//        case totalRaised = "total_raised"
        case quotePreSaleCurrency = "quote_pre_sale_currency"
//        case basePreSaleAmount = "base_pre_sale_amount"
//        case quotePreSaleAmount = "quote_pre_sale_amount"
        case quotePublicSaleCurrency = "quote_public_sale_currency"
        case basePublicSaleAmount = "base_public_sale_amount"
        case quotePublicSaleAmount = "quote_public_sale_amount"
        case acceptingCurrencies = "accepting_currencies"
        case countryOrigin = "country_origin"
//        case preSaleStartDate = "pre_sale_start_date"
//        case preSaleEndDate = "pre_sale_end_date"
        case whitelistURL = "whitelist_url"
//        case whitelistStartDate = "whitelist_start_date"
//        case whitelistEndDate = "whitelist_end_date"
        case bountyDetailURL = "bounty_detail_url"
       // case amountForSale = "amount_for_sale"
        case kycRequired = "kyc_required"
//        case whitelistAvailable = "whitelist_available"
//        case preSaleAvailable = "pre_sale_available"
        case preSaleEnded = "pre_sale_ended"
    }
}

struct gatau: Codable {
    
}

// MARK: - Image
struct image: Codable {
    var thumb, small, large: String?
}

// MARK: - Links
struct Links: Codable {
    var homepage: [String]?
    var blockchainSite, officialForumURL: [String]?
    var chatURL, announcementURL: [String]?
    var twitterScreenName, facebookUsername: String?
    var bitcointalkThreadIdentifier: String?
    var telegramChannelIdentifier: String?
    var subredditURL: String?
    var reposURL: ReposURL?

    enum CodingKeys: String, CodingKey {
        case homepage
        case blockchainSite = "blockchain_site"
        case officialForumURL = "official_forum_url"
        case chatURL = "chat_url"
        case announcementURL = "announcement_url"
        case twitterScreenName = "twitter_screen_name"
        case facebookUsername = "facebook_username"
        case bitcointalkThreadIdentifier = "bitcointalk_thread_identifier"
        case telegramChannelIdentifier = "telegram_channel_identifier"
        case subredditURL = "subreddit_url"
        case reposURL = "repos_url"
    }
}

// MARK: - ReposURL
struct ReposURL: Codable {
    var github: [String]?
    var bitbucket: [String]?
}

//// MARK: - Platforms
//struct Platforms: Codable {
//    //var empty: String?
//
//    enum CodingKeys: String, CodingKey {
//        case empty = ""
//    }
//}

// MARK: - PublicInterestStats
struct PublicInterestStats: Codable {
    var alexaRank: Double?
    var bingMatches: Double?

    enum CodingKeys: String, CodingKey {
        case alexaRank = "alexa_rank"
        case bingMatches = "bing_matches"
    }
}

// MARK: - Description
struct Description: Codable {
    var en: String?
}

