//
//  CryptoListModel.swift
//  Investwatch
//
//  Created by bevan christian on 17/06/21.
//high24H,low24H,priceChange24H,currentPrice,marketCap,totalVolume
//d

import Foundation
import SwiftUI
struct CryptoListModel: Codable,Identifiable {
    var id, symbol, name: String?
    var image: String?
    var currentPrice: Double?
    var marketCap, marketCapRank: Double?
    var fullyDilutedValuation: Double?
    var totalVolume: Double?
    var high24H, low24H, priceChange24H, priceChangePercentage24H: Double?
    var marketCapChange24H, marketCapChangePercentage24H, circulatingSupply: Double?
    var totalSupply, maxSupply: Double?
    var ath, athChangePercentage: Double?
    var athDate: String?
    var atl, atlChangePercentage: Double?
    var atlDate: String?
    var roi: Roi?
    var lastUpdated: String?
    var sparklineIn7D: SparklineIn7D?
    var priceChangePercentage24HInCurrency: Double?
    var favourite = false
    var imageData:UIImage?
    var deskripsi:String?
    var jumlahCoin:String?
    var jumlahDolar:String?
    var hargaRataRata:String?
    var presentasePerubahan:String?
    var jual:Bool?
    var imageUrl:URL {
        return URL(string: image!)!
    }
    

    enum CodingKeys: String, CodingKey {
        case id, symbol, name, image
        case currentPrice = "current_price"
        case marketCap = "market_cap"
        case marketCapRank = "market_cap_rank"
        case fullyDilutedValuation = "fully_diluted_valuation"
        case totalVolume = "total_volume"
        case high24H = "high_24h"
        case low24H = "low_24h"
        case priceChange24H = "price_change_24h"
        case priceChangePercentage24H = "price_change_percentage_24h"
        case marketCapChange24H = "market_cap_change_24h"
        case marketCapChangePercentage24H = "market_cap_change_percentage_24h"
        case circulatingSupply = "circulating_supply"
        case totalSupply = "total_supply"
        case maxSupply = "max_supply"
        case ath = "ath"
        case athChangePercentage = "ath_change_percentage"
        case athDate = "ath_date"
        case atlChangePercentage = "atl_change_percentage"
        case atlDate = "atl_date"
        case roi = "roi"
        case lastUpdated = "last_updated"
        case sparklineIn7D = "sparkline_in_7d"
        case priceChangePercentage24HInCurrency = "price_change_percentage_24h_in_currency"
    }
    
    
//    func getImage(imagekiriman:Data)->CryptoListModel{
//        return CryptoListModel(id: id, symbol: symbol, name: name, image: image, currentPrice: currentPrice, marketCap: marketCap, marketCapRank: marketCapRank, fullyDilutedValuation: fullyDilutedValuation, totalVolume: totalVolume, high24H: high24H, low24H: low24H, priceChange24H: priceChange24H, priceChangePercentage24H: priceChangePercentage24H, marketCapChange24H: marketCapChange24H, marketCapChangePercentage24H: marketCapChangePercentage24H, circulatingSupply: circulatingSupply, totalSupply: totalSupply, maxSupply: maxSupply, ath: ath, athChangePercentage: athChangePercentage, athDate: athDate, atl: atl, atlChangePercentage: atlChangePercentage, atlDate: atlDate, roi: roi, lastUpdated: lastUpdated, sparklineIn7D: sparklineIn7D, priceChangePercentage24HInCurrency: priceChangePercentage24HInCurrency, favourite: favourite,imageData:imagekiriman)
//        }
    }

// MARK: - Roi
struct Roi: Codable {
    var times: Double?
    var currency: String?
    var percentage: Double?
}

// MARK: - SparklineIn7D
struct SparklineIn7D: Codable {
    var price: [Double]?
}



