//
//  Helper.swift
//  Investwatch
//
//  Created by bevan christian on 16/06/21.
//

import Foundation
import SwiftUI

extension Bundle {
    
    func request<T:Codable>(urlString:String,completion:@escaping(T,Bool)->Void){
        guard let url = URL(string: urlString) else {
            fatalError()
        }
        let request = URLRequest(url: url)
        
        URLSession.shared.dataTask(with: request) { Data, response, error in
            guard error == nil else { return }
            let decoder = JSONDecoder()
            if let hasilfix = try? decoder.decode(T.self, from: Data!) {
                completion(hasilfix,false)
            }else {
                print(url)
                print(error?.localizedDescription)
            }
        }.resume()
        
     

    }
    
    
}

extension Color {
    
    static let theme = asset()
}

struct asset {
    var background = Color("background")
    var backgroundDua = Color("background2")
    var utama = Color("fontUtama")
    var kedua = Color("fontkedua")
    var card = Color("card")
    var onboard = Color("onboard")
}
