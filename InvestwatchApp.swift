//
//  InvestwatchApp.swift
//  Investwatch
//
//  Created by bevan christian on 16/06/21.
//

import SwiftUI

@main
struct InvestwatchApp: App {
    @StateObject private var modelData = RequestVM()
    @StateObject private var coreData = coreDataVm()
    @StateObject private var porto = PortofolioVM()
    @StateObject private var detail = DetailVM()
    @StateObject private var transactionData = TransactionVM()
    @Environment(\.scenePhase) var schenePhase
    
    let persistencecontroler = PersistenceControler.shared
    var body: some Scene {
        WindowGroup {
           HalamanUtama().environmentObject(modelData)
            .environmentObject(coreData)
            .environmentObject(transactionData)
            .environmentObject(porto)
            .environmentObject(detail)
            .environment(\.managedObjectContext, persistencecontroler.container.viewContext)
            
        }.onChange(of: schenePhase) { newSchenePhase in
            switch newSchenePhase {
            case .background:
                print("app di background")
                persistencecontroler.save()
            case .inactive:
                print("app tidak aktif")
            case .active:
                print("app aktif")
            @unknown default:
                print("app di default")
            }
        }
    }
}
