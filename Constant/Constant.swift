//
//  Constant.swift
//  Investwatch
//
//  Created by bevan christian on 16/06/21.
//

import Foundation
enum linkPenting {
    case urlList
    case urlBasicIndicator
    case urlDetail(String)
    
    var stringValue: String {
        switch self {
        case .urlList:
            return "https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=200&page=1&sparkline=true&price_change_percentage=24h"
        case .urlDetail(let nama):
            return "https://api.coingecko.com/api/v3/coins/\(nama)?localization=false&tickers=false&market_data=false&developer_data=false&sparkline=false"
        default:
            return "error"
        }
    }
}
